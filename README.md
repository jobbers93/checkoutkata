# checkoutKata

To Run, Navigate to project root directory

Run Command :mvn clean install

Run Command : mvn spring-boot:run

The port can be changed by accessing the application.properties file located in checkoutKata\src\main\resources


---------------------------------------------
Rest API Information

Request Information

GET : http://localhost:9000/item/getAllItems

Returns all added items


POST : http://localhost:9000/item/addItem

Adds an item to the stock list

Request Body 

{
    "id":"z",
    "unitPrice":100
}


POST : http://localhost:9000/offer/addOffer
Adds offer to list
{
    "itemStockId":"z",
    "numberOfUnits":3,
    "price":500
}

GET : http://localhost:9000/offer/getAllOffers
Gets all exsisting offers

GET : http://localhost:9000/checkout/total
Gets total of the current checkout

POST : http://localhost:9000/checkout/item
Adds item to checkout
{ itemIdToCheckout : "z"}

DELETE : http://localhost:9000/checkout/removeAll

Removes all items from checkout